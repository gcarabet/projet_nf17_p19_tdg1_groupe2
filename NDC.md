# Note de clarification

[Relationnel](https://gitlab.utc.fr/-/ide/project/gcarabet/projet_nf17_p19_tdg1_groupe2/edit/master/-/NDC.md#relationnel)  
[MongoDB](https://gitlab.utc.fr/-/ide/project/gcarabet/projet_nf17_p19_tdg1_groupe2/edit/master/-/NDC.md#mongodb)  
[Neo4J](https://gitlab.utc.fr/-/ide/project/gcarabet/projet_nf17_p19_tdg1_groupe2/edit/master/-/NDC.md#neo4j)  
[Relationnel Objet](https://gitlab.utc.fr/-/ide/project/gcarabet/projet_nf17_p19_tdg1_groupe2/edit/master/-/NDC.md#relationnel-objet)  

# Contexte
La clinique vétérinaire a besoin d’un système qui gère les données des clients, du personnel, des animaux, des médicaments et traitements.
Le système à développer doit être capable de traiter ces informations et de faire des requêtes spécifiques sur ces dernières, permettant une utilisation facile et efficace.

# Projet
Le projet consiste en la création d'une base de données pour une clinique vétérinaire.
Cette base de données devra contenir des informations sur les clients, le personnel, les animaux traités et les traitements prescrits.
Cette base de données pourra être manipulée afin de mettre à jour les informations ou d'afficher des statistiques.

On veut pouvoir lister tous les animaux d'un client qui ont été traités dans la clinique.
On veut garder les traitements prescrits par chaque vétérinaire pour chaque animal.

## Relationnel 

#### Hypothèses
* On suppose que l'administrateur ne fait pas partie du personnel soignant,
* On suppose que le personnel n'a qu'une seule spécialité,
* Les medicaments peuvent convenir à une ou plusieurs classes d'espèces animales,

#### Descriptions opérationnelles
* L'administrateur gère la base de données,
* Un client a au moins un animal traité,
* Le personnel ne peut pas avoir d'animal traité,
* Le personnel a un seul poste,
* A chaque membre du personnel est associé une et une seule spécialité,
* Une spécalité est associée à une classe d'espèces animales,
* Une spécialité ne limite pas la prescription d'un traitement à une autre classe d'espèces,
* Un traitement ne peut être prescrit que par un vétérinaire,
* Un traitement contient au moins une molécule,
* Un traitement est prescrit à un seul animal,
* Un médicament n'est autorisé que pour une ou plusieurs espèces,
* Un animal appartient à une seule espèce et donc à une seule classe,
* Un animal est associé à un seul client.

#### Fonctions
* L’application permettra à l’administrateur d’ajouter des clients avec un nom, un prénom, une date
de naissance, une adresse et un numéro de téléphone.
* L’application permettra à l’administrateur d’ajouter du personnel avec un nom, un prénom, une date
de naissance, une adresse, un numéro de téléphone, un poste et à minima une spécialité:
    * Le poste peut prendre la valeur de vétérinaire ou d'assistant,
    * La spécialité ne limite pas le personnel de soigner les autres classes d'espèces,
    * Le personnel à au moins un poste, il n'est pas possible de n'avoir aucun poste. 
* L’application permettra à l’administrateur d’ajouter des animaux, chacun associé à un client, avec un nom, une espèce, un poids et une taille:
    * La date de naissance n’est pas obligatoire,
    * Le client peut être associé à plusieurs animaux, mais un animal qu'à un seul client,
    * L’espèce d’un animal doit appartenir à une seule classe d´espèces animales.
* L’application refusera l'ajout si un client et un personnel ont le même nom, prénom et date de naissance.
* L’application permettra à l’administrateur d’ajouter de nouveaux médicaments avec un
nom, une petite description et la liste des espèces qui peuvent le prendre:
    * Ainsi l’application ne permettra pas ajouter le traitement si l'animal n'appartient pas à l’espèce autorisé pour la prise du médicament.
* L’application permettra de garder un historique des traitements effectués par un vétérinaire, avec les informations suivantes : une date de début, une durée, le nom des médicaments et la quantité journalière prescrite.
    * Un traitement peut contenir plusieurs médicaments. 

## MongoDB

#### Descriptions opérationnelles
* Un client peut avoir zéro ou plusieurs animaux traités,
* Un traitement contient au moins un médicament,
* Un traitement est prescrit pour un seul animal,
* Un animal peut recevoir plusieurs traitements,
* Un animal appartient à une seule espèce et donc à une seule classe,
* Un animal est associé à un seul client.

#### Notas
* Plusieurs solutions sont possibles pour représenter la base :
  * Faire une structure JSON Client avec toutes les informations à l'intérieur -> difficulté à récupérer un animal et les traitements associés à celui-ci,
  * Faire plusieurs structures JSON avec l'id qui réfère à une autre structure pour simuler les "relations" entre les structures -> Les jointures des données devront se faire au niveau applicatif car MongoDB ne le propose pas nativement,

Nous avons choisi d'implémenter plusieurs structures :
* Pour ajouter un animal à un client il faut renseigner la clé naturelle (nom, prenom et ddn) ou la clé artificielle créée par MongoDB dans la structure animal,
* Pour ajouter un traitement à un animal il faut renseigner l'identifiant unique de cet animal car il n'y a pas de clé naturelle, dans la structure traitement,
* Chaque traitement va contenir une liste de médicaments.

## Neo4j

Avec Neo4j nous créons l'ensemble des relations puis nous pourrons choisir lesquelles afficher.
C'est un outil visuel, pratique pour une faible quantité données mais lorsque les relations et noeuds s'accumulent il est très difficile de maintenir la base.

* Nous avons créé plusieurs entités de chaque catégorie (Client, Vétérinaire, Animal, Médicament et Traitement),
* Nous avons ensuite créé les relations entre les entités créées.


#### Attributs des noeuds
Pour exprimer les contraintes de nullité des attributs, pour chaque noeud, on va considerer:
* Tous les attributs des entités Client sont obligatoires,
* Les attributs des entités Animal sont obligatoires, sauf 'naissance',
* Tous les attributs des entités Véterinaire sont obligatoires,
* Tous les attributs des entités Médicament sont obligatoires,
* Les entités de type Traitement n'ont qu'un attribut, qui est clé, et donc il est obligatoire,
* Pour tous les noeuds concernés, les attributs : ddn, debut, durée, annee, taille, poids, naissance, nbParJour sont supérieurs à 0.



#### Contraintes
La base de données orienté graphe ne représente pas bien les contraintes. Il faut les préciser dans un fichier annexe.
Ici nous avons les contraintes suivantes :
* Un client doit avoir au moins un animal traité,
* Un animal n'appartient qu'à un seul client,
* Un animal a reçu, au moins, un traitement,
* Un traitement n'est prescrit qu'à un seul animal,
* Un traitement ne peut être prescrit que par un seul vétérinaire,
* Un traitement comporte au moins un médicament,
* Un médicament n'est pas forcément associé à un traitement,
* Un vétérinaire peut ne pas avoir prescrit de traitement,
* Les médicaments d'un traitement n'ont pas forcément la même date de debut (il y a des médicaments qui peuvent être pris après d'autres),
* La durée des médicaments d'un même traitement n'est pas forcément la même.


#### Sens des liens
Afin de nommer les liens, on a placé des mots descriptifs sur ces derniers.  
Les relations possibles sont décrites selon les labels suivants:
* Un client est proprietaire d'un animal:            CLIENT   --proprietaireDe --> ANIMAL
* Un médicament fait partie d'un traitement:         MÉDICAMENT  --appartientA --> TRAITEMENT
* Un traitement est prescrit par un véterinaire:     TRAITEMENT --prescritPar-->  VÉTERINAIRE
* Un traitement est prescrit pour un animal:         TRAITEMENT --prescritA--> ANIMAL

## Relationnel Objet

#### Hypothèses
* On suppose qu'un médicament n'est plus réservé à une espèce,
* Un traitement est prescrit avec un seul médicament à la fois,
* On suppose que les Clients peuvent avoir deux numéros de téléphones (domicile et portable).

#### Contraintes
* Un vétérinaire a prescrit au moins un traitement pour qu'il soit présent dans la BDD,
* Un animal a reçu au moins un traitement pour être placé dans la BDD,
  * Donc au moins un médicament a été prescrit.

#### Choix du modèle
* Nous avons intégré les classes d'espèces dans la table espèce pour réduire l'espace mémoire occupé et éviter de faire des jointures au niveau applicatif,
* Nous avons choisit d'imbriquer les tables Vétérinaire, Médicament et Posologie dans la table Traitement,
* Nous avons imbriqué la table Traitement à la table Animal,
* Nous avons rajouter une table Numéro, imbriquée dans la table Client.

#### Pertes du modèle
* On perd l'identification des médicaments et des vétérinaires :
  * Un vétérinaire n'existe dans la base que lorsqu'il a prescrit au moins un traitement,
  * Un médicament n'existe dans la base que lorsqu'il a été prescrit à un animal.
* On perd l'identification des traitements car ils sont directement intégrés aux animaux,
* On ne pourra pas connaître les traitements prescrit par un vétérinaire (plus d'identification)

#### Gains du modèle
* On rajoute des informations à l'animal :
  * On pourra récupérer plus rapidement les traitements prescrit à un animal,
  * On pourra connaître tous les vétérinaires qui ont prescrit un traitement à cet animal,
  * On pourra connaître tous les médicaments prescrit.
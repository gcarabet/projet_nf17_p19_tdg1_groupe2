@startuml
hide circle
hide empty members
skinparam linetype ortho

note "On suppose qu'un traitement\n n'est prescrit que par un\n seul veterinaire." as N1
note "On suppose qu'un médicament\n correspond à une molécule" as N2
note "Tous les héritages sont exclusifs" as NGeneral1
note "Tous les attributs sont NOT NULL par défaut" as NGeneral2

class Personne <<Abstract>> {
	nom : string
	prenom : string
	date de naissance : date
	adresse : string
	numero de telephone : int
}
note left of Personne
	Le numéro de téléphone est
	composé de 10 chiffres.
	(nom, prenom, DDN) {key}
endnote

class Client
class Personnel <<Abstract>> {
	specialité : string 
}
note left of Personnel
	La valeur de spécialité doit
	correspondre à une des valeurs
	de nomClasse de la ClasseEspece
endnote

class Veterinaire
class Assistant

class Classe_espece {
	nomClasse : string {key}
}

class Espece {
	nomEspece : string {key}
}

class Animal {
	nom : string
	poids : float
	taille : float
	naissance : date
}
note left of Animal
	date de naissance peut etre NULL
	poids>0
	taille>0
endnote

class Traitement

class Posologie {
	debut : date
	duree : int
	nbJournalier : int
}
note right of Posologie
	duree > 0
	nbJournalier > 0
endnote

class Medicament {
	nomMolec : string {key}
	description : string
	espece_autoriser() : bool
}
note bottom of Medicament
	Si l'animal peut prendre
	le médicament, espece_autoriser()
	renvoit vrai. Faux sinon.
endnote
Medicament .. N2

Personne <|-- Personnel
Personne <|-- Client

Personnel <|-- Veterinaire
Personnel <|-- Assistant

' Association
Traitement "0..*" -- "1" Animal : prescrit à >

' Association
Veterinaire "1" -- "0..*" Traitement : prescrit >
Veterinaire .. N1
N1 .. Traitement

' Association
Animal "1..*" -- "1" Client : possede <

' Association et groupe d'association
Traitement "0..*" -- "1..*" Medicament : liste >
(Traitement, Medicament) .. Posologie

' Association modéliser dans un attribut de Personnel
' Personnel "1..*" -- "1" Classe_espece : spécialisé >

' Composition class_espece et espece
Classe_espece -- "1..*" Espece : appartient <

Espece "1..*" -- "1..*" Medicament : peut prendre >

' Association
Espece -- "1..*" Animal : appartient <

@enduml

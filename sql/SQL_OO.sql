DROP TABLE Client;
DROP TYPE ListeRefAnimal;
DROP TYPE RefAnimal;
DROP TABLE tAnimal;
DROP TYPE Animal;
DROP TYPE ListeRefTraitement;
DROP TYPE RefTraitement;
DROP TABLE tTraitement;
DROP TYPE Traitement FORCE;
DROP TYPE listePosologies;
DROP TYPE Posologie;
DROP TABLE tMedicament;
DROP TYPE Medicament;
DROP TYPE ListeRefEspece;
DROP TYPE RefEspece;
DROP TABLE tEspece;
DROP TYPE Espece;
DROP TABLE Veterinaire;
DROP TABLE Assistant;
DROP TABLE tClasseEspece;
DROP TYPE ClasseEspece;

CREATE OR REPLACE TYPE ClasseEspece AS OBJECT (
  nomClasse VARCHAR(20)
);
/

CREATE TABLE tClasseEspece OF ClasseEspece (
  PRIMARY KEY (nomClasse)
);
/

CREATE TABLE Assistant (
  idAssistant INTEGER PRIMARY KEY,
  nom VARCHAR (20) NOT NULL,
  prenom VARCHAR(20) NOT NULL,
  ddn DATE NOT NULL,
  numero VARCHAR (10) NOT NULL,
  specialite REF ClasseEspece,
  SCOPE FOR (specialite) IS tClasseEspece,
  CHECK (numero BETWEEN 0100000000 AND 0799999999)
);
/


CREATE TABLE Veterinaire (
  idVeterinaire INTEGER PRIMARY KEY,
  nom VARCHAR (20) NOT NULL,
  prenom VARCHAR(20) NOT NULL,
  ddn DATE NOT NULL,
  numero VARCHAR (10) NOT NULL,
  specialite REF ClasseEspece,
  SCOPE FOR (specialite) IS tClasseEspece,
  CHECK (numero BETWEEN 0000000000 AND 0799999999)
);
/



CREATE OR REPLACE TYPE Espece AS OBJECT (
  espece VARCHAR(20),
  classe REF ClasseEspece
);
/


CREATE TABLE tEspece OF Espece (
  PRIMARY KEY (espece),
  classe NOT NULL,
  SCOPE FOR (classe) IS tClasseEspece
);
/


CREATE OR REPLACE TYPE RefEspece AS OBJECT (refEspece REF Espece);
/
CREATE OR REPLACE TYPE ListeRefEspece AS TABLE OF RefEspece;
/

CREATE OR REPLACE TYPE Medicament AS OBJECT(
  nomMolec VARCHAR(20),
  descriptions VARCHAR(100),
  medEspeces ListeRefEspece,
  MEMBER FUNCTION espece_autoriser(species IN VARCHAR) RETURN BOOLEAN
);
/

CREATE TABLE tMedicament OF Medicament(
  PRIMARY KEY (nomMolec),
  descriptions NOT NULL
) NESTED TABLE medEspeces STORE AS ntRefEspeces;
/

/*
CREATE OR REPLACE TYPE BODY Medicament
IS
MEMBER FUNCTION espece_autoriser(species VARCHAR) RETURN BOOLEAN
    IS
      resultat BOOLEAN;
      espe Espece;
      found VARCHAR(20);
    BEGIN
      SELECT REF(te) INTO espe
      FROM tEspece te
      WHERE te.espece = species;
      
      SELECT med.nommolec INTO found
      FROM tMedicament med
      WHERE med.medespeces = espe;

    IF found THEN
      resultat := True;
    ELSE
      resultat := False;
    END IF;
    RETURN resultat;
  END;
END;
/
*/

CREATE OR REPLACE TYPE Posologie AS OBJECT(
    nomMolec REF Medicament,
    debut DATE,
    duree INTEGER,
    nbJours INTEGER
);
/


CREATE OR REPLACE TYPE listePosologies AS TABLE OF Posologie;
/


CREATE OR REPLACE TYPE Traitement AS OBJECT (
  idTraitement INTEGER,
  idVeterinaire INTEGER,
  l_posologie listePosologies
);
/

CREATE TABLE tTraitement OF Traitement (
  PRIMARY KEY (idTraitement),
  FOREIGN KEY (idVeterinaire) REFERENCES Veterinaire(idVeterinaire)
) NESTED TABLE l_posologie STORE AS ntPosologies;
/

CREATE OR REPLACE TYPE RefTraitement AS OBJECT (refTraitement REF Traitement);
/

CREATE OR REPLACE TYPE ListeRefTraitement AS TABLE OF RefTraitement;
/


CREATE OR REPLACE TYPE Animal AS OBJECT(
    idAnimal INTEGER,
    nom VARCHAR(20),
    poids INTEGER,
    taille INTEGER,
    naissance DATE,
    l_traitements ListeRefTraitement,
    esp REF Espece
);
/

CREATE TABLE tAnimal OF Animal (
    PRIMARY KEY (idAnimal),
    nom NOT NULL,
    poids NOT NULL,
    taille NOT NULL,
    SCOPE FOR (esp) IS tEspece,
    CHECK (poids>0),
    CHECK (taille>0)
) NESTED TABLE l_traitements STORE AS ntTraitements;
/

CREATE OR REPLACE TYPE RefAnimal AS OBJECT (refAnimal REF Animal);
/

CREATE OR REPLACE TYPE ListeRefAnimal AS TABLE OF RefAnimal;
/


CREATE TABLE Client (
    idClient INTEGER PRIMARY KEY,
    nom VARCHAR (20) NOT NULL,
    prenom VARCHAR(20) NOT NULL,
    ddn DATE NOT NULL,
    adresse VARCHAR(20) NOT NULL,
    numero VARCHAR(10) NOT NULL,
    CHECK (numero BETWEEN 0100000000 AND 0799999999),
    l_animal ListeRefAnimal
) NESTED TABLE l_animal STORE AS ntAnimaux;
/


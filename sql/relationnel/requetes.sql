/* Information :

    Les variables sont notées de cette façon : $nomVariable

*/

/* Requête pour la quantité de médicament prescrite
  à un animal */
SELECT P.nomMolec as medicament, COUNT(*) as quantite_prescrite
FROM Posologie P JOIN Traitement T on P.traitement=T.idTraitement
WHERE T.idAnimal= $idAnimal
GROUP BY P.nomMolec;

/* Requete pour le nombre de comprimé prescrit
  à un animal */
select P.nomMolec,SUM(P.duree*P.nbJourna) FROM Posologie P 
JOIN Traitement T ON P.traitement=T.idTraitement WHERE 
T.idAnimal=$idAnimal  group by P.nomMolec;


/***********************************/

/* Requête pour la quantité de médicament prescrite
  dans la clinique */
SELECT P.nomMolec as medicament, COUNT(*) as quantite_prescrite
FROM Posologie P
GROUP BY P.nomMolec;

/* Requete pour le nombre de comprimé prescrit
  dans la clinique */
SELECT CNT.medicament, sum(CNT.nb) as nombre_comprime
FROM (SELECT P.nomMolec as medicament, COUNT(*)*P.duree*P.nbJourna as nb
  FROM Posologie P
  GROUP BY P.nomMolec, P.duree, P.nbJourna) as CNT
GROUP BY CNT.medicament

/***********************************/

/* Requête pour la quantité total d'un médicament
  prescrit dans la clinique */
SELECT P.nomMolec as medicament, COUNT (*) as quantite_medicament
FROM Posologie P
WHERE P.nomMolec = $nomMolec
GROUP BY P.nomMolec;

/* Requete pour le nombre de comprimé d'un medicament
  prescrit dans la clinique */
SELECT CNT.medicament, sum(CNT.nb) as nombre_comprime
FROM (SELECT P.nomMolec as medicament, COUNT(*)*P.duree*P.nbJourna as nb
  FROM Posologie P
  WHERE P.nomMolec = $nomMolec
  GROUP BY P.nomMolec, P.duree, P.nbJourna) as CNT
GROUP BY CNT.medicament;

/***********************************/

/* Requête pour le poids moyen d'une espèce traité
  dans la clinique */
SELECT A.especenom as Espece, AVG(A.poids) as Poids_Moyen
FROM Animal A
WHERE A.especenom = $especeNom
GROUP BY A.especenom;

/* Requête pour la taille moyenne d'une espèce traité
  dans la clinique */
SELECT A.especenom as Espece, AVG(A.taille) as Taille_Moyenne
FROM Animal A
WHERE especenom = $especenom
GROUP BY A.especenom;

/* On peut en déduire des deux requêtes précédentes
  des requêtes pour l'ensemble des espèces
  de la clinique */

/* Requete pour le poids moyen et la taille moyenne de toutes les especes
  traitées dans la clinique */
SELECT A.especenom as Espece, AVG(a.poids) as Poids_Moyen, AVG(A.taille) as Taille_Moyenne
FROM Animal A
GROUP BY A.especenom;


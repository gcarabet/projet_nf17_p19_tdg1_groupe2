CREATE TABLE ClasseEspece(
	nomClasse VARCHAR(30) NOT NULL,
	PRIMARY KEY(nomClasse)
); 

CREATE TABLE Client(
	idClient INT NOT NULL,
	nom VARCHAR(30) NOT NULL,
	prenom VARCHAR(30) NOT NULL,
	ddn DATE NOT NULL,
	adresse VARCHAR(100) NOT NULL,
	numero INT NOT NULL,
	PRIMARY KEY(idClient),
  CHECK (numero >= 0100000000 AND numero <= 0799999999)
);

CREATE TABLE Assistant(
	idAssistant INT NOT NULL,
	nom VARCHAR(30) NOT NULL,
	prenom VARCHAR(30) NOT NULL,
	ddn DATE NOT NULL,
	adresse VARCHAR(100) NOT NULL,
	numero INT NOT NULL,
	specialite VARCHAR(30),
	PRIMARY KEY(idAssistant),
  FOREIGN KEY(specialite) REFERENCES ClasseEspece(nomClasse),
  CHECK (numero >= 0100000000 AND numero <= 0799999999)
); 

CREATE TABLE Veterinaire(
	idVeterinaire INT NOT NULL,
	nom VARCHAR(30) NOT NULL,
	prenom VARCHAR(30) NOT NULL,
	ddn DATE NOT NULL,
	adresse VARCHAR(100) NOT NULL,
	numero INT NOT NULL,
	specialite VARCHAR(30),
	PRIMARY KEY(idVeterinaire),
  FOREIGN KEY(specialite) REFERENCES ClasseEspece(nomClasse),
  CHECK (numero >= 0100000000 AND numero <= 0799999999)
); 

CREATE TABLE Espece(
	nomEspece VARCHAR(30) NOT NULL,
	nomClasse VARCHAR(30) NOT NULL,
	PRIMARY KEY(nomEspece),
	FOREIGN KEY(nomClasse) REFERENCES ClasseEspece(nomClasse)
); 

CREATE TABLE Animal(
	idAnimal INT NOT NULL,
	nom VARCHAR(30) NOT NULL,
	poids REAL NOT NULL,
	taille REAL NOT NULL,
	naissance DATE,
	idClient INT NOT NULL,
	especeNom VARCHAR(30),
	PRIMARY KEY(idAnimal),
	FOREIGN KEY (idClient) REFERENCES Client(idClient),
	FOREIGN KEY (especeNom) REFERENCES Espece(nomEspece),
	CHECK (POIDS > 0),
  CHECK (TAILLE > 0)
); 

CREATE TABLE Medicament(
	nomMolec VARCHAR(30) NOT NULL,
	description VARCHAR(200) NOT NULL,
	PRIMARY KEY(nomMolec)
); 

CREATE TABLE Traitement(
	idTraitement INT NOT NULL,
	idAnimal INT NOT NULL,
	idVeterinaire INT NOT NULL,
	PRIMARY KEY(idTraitement),
	FOREIGN KEY(idVeterinaire) REFERENCES Veterinaire(idVeterinaire),
	FOREIGN KEY(idAnimal) REFERENCES Animal(idAnimal)
); 

CREATE TABLE Espece_Med(
	nomEspece VARCHAR(30) NOT NULL,
	nomMolec VARCHAR(30) NOT NULL,
  PRIMARY KEY(nomEspece, nomMolec),
	FOREIGN KEY(nomEspece) REFERENCES Espece(nomEspece),
	FOREIGN KEY(nomMolec) REFERENCES Medicament(nomMolec)
); 

CREATE TABLE Posologie(
	idAnimal INT NOT NULL REFERENCES Animal(idAnimal),
	nomMolec VARCHAR(30) NOT NULL REFERENCES Medicament(nomMolec),
	debut DATE NOT NULL,
	duree INT NOT NULL,
	nbJourna INT NOT NULL,
	PRIMARY KEY(idAnimal, nomMolec, debut),
  CHECK (duree > 0),
  CHECK (nbJourna > 0)
); 

/* Methodes */
/* Non fonctionnelles */
/*
CREATE FUNCTION naissance (Naissance date)
returns date
as
begin
  declare(@nais date)
  set @nais =
  case Naissance
    when Naissance then Naissance
    when NOT(Naissance) then NULL
  end--case
  return @nais
end;

CREATE FUNCTION espece_autoriser (Animal string, nomMolec string)
  return (
  SELECT CASE WHEN EXISTS (
  SELECT *
  FROM Espece_Med E JOIN Animal A
  ON E.nomEspece = A.especeNom
  AND A.idAnimal = Animal
  WHERE  E.nomMolec = nomMolec
)
THEN CAST(1 AS BIT)
ELSE CAST(0 AS BIT)
END -- CASE
)
END;

*/

/* Vues
vPersonne(Union(Projection(Client, nom, prenom, ddn, adresse, numero), Union(Projection(Assistant, nom, prenom, ddn, adresse, numero, specialite), Projection(Veterinaire, nom, prenom, ddn, adresse, numero, specialite))))
vPersonnel(Union(Projection(Assistant, nom, prenom, ddn, adresse, numero, specialite), Projection(Veterinaire, nom, prenom, ddn, adresse, numero, specialite)))
*/

/* à tester */

/*

CREATE OR REPLACE VIEW Personne AS
SELECT C.nom, C.prenom, C.ddn, C.adresse, C.numero
FROM Client C
UNION ALL
(
SELECT A.nom, A.prenom, A.ddn, A.adresse, A.numero
  FROM Assistant A
  UNION ALL
  SELECT V.nom, V.prenom, V.ddn, V.adresse, V.numero
  FROM Veterinaire V);

CREATE OR REPLACE VIEW Personnel AS
  SELECT A.nom, A.prenom, A.ddn, A.adresse, A.numero, A.specialite
  FROM Assistant A
  UNION ALL
  SELECT V.nom, V.prenom, V.ddn, V.adresse, V.numero, V.specialite
  FROM Veterinaire V;

  */

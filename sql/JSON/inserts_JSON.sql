INSERT INTO classeEspece (nomClasse) VALUES ('mammifere');
INSERT INTO classeEspece (nomClasse) VALUES ('reptile');
INSERT INTO classeEspece (nomClasse) VALUES ('oiseau');
INSERT INTO classeEspece (nomClasse) VALUES ('autres');

INSERT INTO Client (idClient,nom,prenom,ddn,adresse,numero) VALUES (1,'Harlan','Wise',to_date('1970-09-07', 'YYYY-MM-DD'),'Apdo.:630-5180 Fusce Avenida','0132102368');
INSERT INTO Client (idClient,nom,prenom,ddn,adresse,numero) VALUES (2,'Lee','Munoz',to_date('1985-01-31', 'YYYY-MM-DD'),'5900 Faucibus ','0721229832');
INSERT INTO Client (idClient,nom,prenom,ddn,adresse,numero) VALUES (3,'Gisela','Zamora',to_date('1984-06-18', 'YYYY-MM-DD'),'2165 Donec Avda.','0715456201');

INSERT INTO Espece (nomEspece,classe) VALUES ('hamster','mammifere');
INSERT INTO Espece (nomEspece,classe) VALUES ('chat','mammifere');
INSERT INTO Espece (nomEspece,classe) VALUES ('lapin','mammifere');
INSERT INTO Espece (nomEspece,classe) VALUES ('chien','mammifere');
INSERT INTO Espece (nomEspece,classe) VALUES ('souris','mammifere');
INSERT INTO Espece (nomEspece,classe) VALUES ('moineau', 'oiseau');
INSERT INTO Espece (nomEspece,classe) VALUES ('tortue','reptile');
INSERT INTO Espece (nomEspece,classe) VALUES ('dragon', 'autres');

INSERT INTO animal VALUES (
  1,'Nita',3079,62,to_date('2018-07-26', 'YYYY-MM-DD'),
  '[
    {
      "veterinaire":
      {
        "nom": "MURPHY",
        "prenom": "Carl"
      },
      "medicaments":[
        {
          "nom": "LEVOTHYROX",
          "posologie":
          {
            "debut": "2019-06-17",
            "duree": 10,
            "quantité": 4
          }
        },
        {
          "nom": "DOLIPRANE",
          "posologie":
          {
            "debut": "2019-06-17",
            "duree": 10,
            "quantité": 3
          }
        }
      ]
    },
    {
      "veterinaire":
      {
        "nom": "ZAMORA",
        "prenom": "Gisela"
      },
      "medicaments":[
        {
          "nom": "LEVOTHYROX",
          "posologie":
          {
            "debut": "2019-06-17",
            "duree": 10,
            "quantité": 4
          }
        },
        {
          "nom": "DOLIPRANE",
          "posologie":
          {
            "debut": "2019-06-17",
            "duree": 10,
            "quantité": 3
          }
        },
        {
          "nom": "LEVOTHYROX",
          "posologie":
          {
            "debut": "2019-06-17",
            "duree": 10,
            "quantité": 2
          }
        }
      ]
    }
  ]', 1,'lapin');



  INSERT INTO animal VALUES (
    2,'Tanisha',4107,12,to_date('2018-06-11', 'YYYY-MM-DD'),
  '[
    {
      "veterinaire":
      {
        "nom": "ZAMORA",
        "prenom": "Gisela"
      },
      "medicaments":[
        {
          "nom": "IMODIUM",
          "posologie":
          {
            "debut": "2019-06-17",
            "duree": 10,
            "quantité": 2
          }
        },
        {
          "nom": "FORLAX",
          "posologie":
          {
            "debut": "2019-06-17",
            "duree": 10,
            "quantité": 3
          }
        }
      ]
    }
  ]', 2,'lapin');


    INSERT INTO animal VALUES (
      3,'Rajah',3221,28,to_date('2011-10-12', 'YYYY-MM-DD'),
  '[
    {
      "veterinaire":
      {
        "nom": "ZAMORA",
        "prenom": "Gisela"
      },
      "medicaments":[
        {
          "nom": "PLAVIX",
          "posologie":
          {
            "debut": "2019-06-17",
            "duree": 10,
            "quantité": 2
          }
        }
      ]
    }
  ]', 3,'hamster');

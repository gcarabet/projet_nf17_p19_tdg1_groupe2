/*
Exemple de requête pour récupérer des éléments
 d'un tableau dans un tableau JSON
*/
select m->>'nom' 
from animal a, json_array_elements(a.traitements) t, json_array_elements(t->'medicaments') m;

drop table animal;
drop table espece;
drop table client;
drop type listTraitements;
drop type listNumeros;
drop type traitement;

CREATE TYPE traitement AS OBJECT (
    id_traitement NUMBER,
    debut DATE,
    duree NUMBER,
    quantite NUMBER,
    veterinaire VARCHAR(30),
    medicament VARCHAR(30)
);
/


CREATE TYPE listNumeros AS TABLE OF VARCHAR(10);
/

CREATE TYPE listTraitements AS TABLE OF traitement;
/


CREATE TABLE client (
    id_client NUMBER,
    nom VARCHAR(20) NOT NULL,
    prenom VARCHAR(20) NOT NULL,
    dn DATE NOT NULL,
    adresse VARCHAR(50) NOT NULL,
    numeros listNumeros,
    PRIMARY KEY(id_client)
)NESTED TABLE numeros STORE AS l_numeros;
/

CREATE  TABLE espece(
    classeEspece VARCHAR(30) NOT NULL,
    espece VARCHAR(20),
    PRIMARY KEY(espece)
);
/

CREATE  TABLE animal(
    id_animal NUMBER,
    id_client NUMBER,
    espece VARCHAR(20),
    nom VARCHAR(20),
    poid FLOAT,
    TAILLE FLOAT,
    dn DATE,
    traitements listTraitements,
    CHECK (poid > 0),
    CHECK (TAILLE > 0),
    PRIMARY KEY (id_animal),
    FOREIGN KEY (id_client) REFERENCES client(id_client),
    FOREIGN KEY (espece) REFERENCES espece(espece)
) NESTED TABLE traitements STORE AS l_traitements;
/
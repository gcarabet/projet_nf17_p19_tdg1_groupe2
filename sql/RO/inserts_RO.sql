
INSERT INTO espece (classeEspece,espece)
VALUES ('mammifer','hamster');
/

INSERT INTO espece (classeEspece,espece)
VALUES ('mammifer','chien');
/

INSERT INTO espece (classeEspece,espece)
VALUES ('mammifer','chat');
/

INSERT INTO client (id_client,nom,prenom,dn,adresse,numeros)
VALUES (1,'VALDIVIA','Osvaldo',to_date('27-12-1998','DD-MM-YYYY'),'6 bis rue Winston Churchill',listNumeros('1029384756','5534034564'));
/


INSERT INTO client (id_client,nom,prenom,dn,adresse,numeros)
VALUES (2,'CARABETTA','Gaetan',to_date('10-06-1996','DD-MM-YYYY'),'16 rue Gambeta',listNumeros('5647384950','1738493728','5748394782'));
/


INSERT INTO client (id_client,nom,prenom,dn,adresse,numeros)
VALUES (3,'ALANIS','Oswaldo',to_date('14-05-1998','DD-MM-YYYY'),'52 av. Compiegne ',listNumeros('5647384950'));
/

INSERT INTO animal(id_animal,id_client,espece,nom,poid,TAILLE,dn,traitements)
VALUES (1,1,'chien','Diego',4050,300,to_date('15-04-2005','DD-MM-YYYY'),listTraitements(traitement(1,to_date('09-10-2018','DD-MM-YYYY'),5,2,'Pierre','Hueys'),traitement(2,to_date('11-12-2018','DD-MM-YYYY'),5,2,'Pierre','Orphex')));


INSERT INTO animal(id_animal,id_client,espece,nom,poid,TAILLE,dn,traitements)
VALUES (2,2,'chat','Doris',2100,200,to_date('15-02-2008','DD-MM-YYYY'),listTraitements(traitement(3,to_date('01-10-2019','DD-MM-YYYY'),4,1,'Jean','Hueys'),traitement(4,to_date('02-12-2019','DD-MM-YYYY'),6,3,'Pierre','Orphex')));


INSERT INTO animal(id_animal,id_client,espece,nom,poid,TAILLE,dn,traitements)
VALUES (3,3,'hamster','Pochoclo',100,10,to_date('10-10-2018','DD-MM-YYYY'),listTraitements(traitement(5,to_date('15-01-2019','DD-MM-YYYY'),4,1,'Valentin','Marphel')));
INSERT INTO classeEspece (nomClasse) VALUES ('mammifere');
INSERT INTO classeEspece (nomClasse) VALUES ('reptile');
INSERT INTO classeEspece (nomClasse) VALUES ('oiseau');
INSERT INTO classeEspece (nomClasse) VALUES ('autres');


INSERT INTO client (idClient,nom,prenom,ddn,adresse,numero) VALUES (1,'Harlan','Wise',to_date('1970-09-07', 'YYYY-MM-DD'),'Apdo.:630-5180 Fusce Avenida','0132102368');
INSERT INTO client (idClient,nom,prenom,ddn,adresse,numero) VALUES (2,'Lee','Munoz',to_date('1985-01-31', 'YYYY-MM-DD'),'5900 Faucibus ','0721229832');
INSERT INTO client (idClient,nom,prenom,ddn,adresse,numero) VALUES (3,'Gisela','Zamora',to_date('1984-06-18', 'YYYY-MM-DD'),'2165 Donec Avda.','0715456201');


INSERT INTO espece (nomEspece,nomClasse) VALUES ('hamster','mammifere');
INSERT INTO espece (nomEspece,nomClasse) VALUES ('chat','mammifere');
INSERT INTO espece (nomEspece,nomClasse) VALUES ('lapin','mammifere');
INSERT INTO espece (nomEspece,nomClasse) VALUES ('chien','mammifere');
INSERT INTO espece (nomEspece,nomClasse) VALUES ('souri','mammifere');
INSERT INTO espece (nomEspece,nomClasse) VALUES ('tortue','autres');



INSERT INTO animal VALUES (
  1,'Nita',3079,62,to_date('2018-07-26', 'YYYY-MM-DD'),
  XMLTYPE('<traitements>
                <traitement>
                      <veterinaire>
                          <nom> MURPHY </nom>
                          <prenom> Carl </prenom>
                      </veterinaire>
                      <medicaments>
                          <posologie>
                            <qte> 4 </qte>
                            <medicament>
                                <nomMolec> LEVOTHYROX </nomMolec>
                            </medicament>
                          </posologie>
                          <posologie>
                            <qte> 3 </qte>
                            <medicament>
                                <nomMolec> DOLIPRANE </nomMolec>
                            </medicament>
                          </posologie>
                      </medicaments>
                </traitement>
                <traitement>
                      <veterinaire>
                          <nom> ZAMORA </nom>
                          <prenom> Gisela </prenom>
                      </veterinaire>
                      <medicaments>
                          <posologie>
                            <qte> 2 </qte>
                            <medicament>
                                <nomMolec> LEVOTHYROX </nomMolec>
                            </medicament>
                          </posologie>
                      </medicaments>
                </traitement>
          </traitements>'),
  1,'lapin');



  INSERT INTO animal VALUES (
    2,'Tanisha',4107,12,to_date('2018-06-11', 'YYYY-MM-DD'),
    XMLTYPE('<traitements>
                    <traitement>
                        <veterinaire>
                            <nom> ZAMORA </nom>
                            <prenom> Gisela </prenom>
                        </veterinaire>
                        <medicaments>
                            <posologie>
                              <qte> 2 </qte>
                              <medicament>
                                  <nomMolec> IMODIUM </nomMolec>
                              </medicament>
                            </posologie>
                            <posologie>
                              <qte> 3 </qte>
                              <medicament>
                                  <nomMolec> FORLAX </nomMolec>
                              </medicament>
                            </posologie>
                        </medicaments>
                    </traitement>
            </traitements>'),
    2,'lapin');


    INSERT INTO animal VALUES (
      3,'Rajah',3221,28,to_date('2011-10-12', 'YYYY-MM-DD'),
      XMLTYPE('<traitements>
                    <traitement>
                          <veterinaire>
                              <nom> ZAMORA </nom>
                              <prenom> Gisela </prenom>
                          </veterinaire>
                          <medicaments>
                              <posologie>
                                <qte> 2 </qte>
                                <medicament>
                                    <nomMolec> PLAVIX </nomMolec>
                                </medicament>
                              </posologie>
                          </medicaments>
                    </traitement>
              </traitements>'),
      3,'hamster');

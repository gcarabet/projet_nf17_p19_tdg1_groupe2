CREATE TABLE Client(
  idClient INT NOT NULL,
	nom VARCHAR(30) NOT NULL,
	prenom VARCHAR(30) NOT NULL,
	ddn DATE NOT NULL,
	adresse VARCHAR(100) NOT NULL,
	numero INT NOT NULL,
	PRIMARY KEY(idClient),
  CHECK (numero >= 0100000000 AND numero <= 0799999999)
);

CREATE TABLE classeEspece(
  classeEspece VARCHAR(30) NOT NULL,
   PRIMARY KEY (classeEspece)
);


CREATE TABLE Espece(
  classe VARCHAR(30) NOT NULL,
  nomEspece VARCHAR(30) NOT NULL,
  PRIMARY KEY (nomEspece), 
  FOREIGN KEY(classe) REFERENCES classeEspece(classeEspece);
);

CREATE TABLE Animal (
  idAnimal INT NOT NULL,
  nom VARCHAR(30) NOT NULL,
  poids REAL NOT NULL,
  taille REAL NOT NULL,
  naissance DATE,
  traitements XMLTYPE,
  idClient INT NOT NULL,
  especeNom VARCHAR(30),
  PRIMARY KEY(idAnimal),
  FOREIGN KEY (idClient) REFERENCES Client(idClient),
  FOREIGN KEY (especeNom) REFERENCES Espece(nomEspece),
  CHECK (POIDS > 0),
  CHECK (TAILLE > 0)
);

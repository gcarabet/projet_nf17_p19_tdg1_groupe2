# Projet : Clinique vétérinaire
projet_nf17_p19_tdg1_groupe2

### Auteurs :
---
* Gaetan CARABETTA
* Osvaldo VALDIVIA SALAS
* Oswaldo Aldair ALANIS MAYORGA
* Saada BRAHITI

### Informations :
---
```
README.md : Ce fichier  
NDC.md : Note de clarification  
Normalisation.txt : Fichier contenant les DF, DFE, CM et analyses de normalisation  
mongo.json : Requêtes pour créer la structure de la BDD  
Neo4j_Proj : Requêtes de création de la BDD en graphe  

mcd/
| -MCD.uml : Modèle conceptuel de donnée (PlantUML)  
| -UML MongoDB : Modèle UML pour MongoDB  
| -UML Neo4j : Modèle UML pour Neo4j  
mld/
| -MLD : Modèle relationnel  
| -MLD MongoDB : Modélisation logique pour MongoDB  
| -MLD Neo4j : Modélisation logique pour Neo4j
sql/  
| -create.sql : Création de la base de données  
| -inserts.sql : Insertion de données  
| -drop.sql : Destruction de la base de donnée  
| -requetes.sql : Requêtes SQL pour afficher des statistiques
```
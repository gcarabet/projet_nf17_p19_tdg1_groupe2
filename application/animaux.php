<html>
<head>
  <title>Clinique Vétérinaire</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="stylesheet" type="text/css" href="style.css">
  </head>
<body>
  <h1>Information sur les animaux</h1>
  <table border="1">
    <tr>
      <th>idAnimal</th>
      <th>Nom</th>
      <th>Poids</th>
      <th>Taille</th>
      <th>Naissance</th>
      <th>idClient</th>
      <th>Espece</th>
      <th>Statistiques</th>
      <th>Supprimer</th>
    </tr>
    <?php
    // Connexion à la base de données
    include("connexion.php");

    try{
      $vConn = new PDO("pgsql:host=$vHost;port=$vPort;dbname=$vData", $vUser, $vPass);
    } catch(PDOException $e){
      echo "Erreur de connexion '$e'";
    }

    // Affichage du tableau de données
    $vSql = "select * from animal";
    try{
      $vSt = $vConn->prepare($vSql);
      $vSt->execute();
      
      while ($vResult = $vSt->fetch(PDO::FETCH_BOTH)) {
        echo '<tr>';
        echo "<td>$vResult[0]</td>";
        echo "<td>$vResult[1]</td>";
        echo "<td>$vResult[2]</td>";
        echo "<td>$vResult[3]</td>";
        echo "<td>$vResult[4]</td>";
        echo "<td>$vResult[5]</td>";
        echo "<td>$vResult[6]</td>";
        echo "<td><form action='statAnimal.php' method='post'> <input type='text' name='idAnimal' value=$vResult[0] hidden/> <input  type='submit' value='Statistiques'> </form></td>";
        echo "<td><form action='remAnimal.php' method='post'> <input type='text' name='idAnimal' value=$vResult[0] hidden/> <input id='btnRemove' type='submit' value='X'> </form></td>";
        echo '</tr>';
      }

    } catch(PDOException $e){
      echo "Erreur de requete '$e'";
    }
    ?>

  </table>
</body>
</html>

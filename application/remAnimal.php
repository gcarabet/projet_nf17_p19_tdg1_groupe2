<html>
<head>
  <title>Exercice</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  </head>
<body>
  <?php
    include("connexion.php");

    try{
      $vConn = new PDO("pgsql:host=$vHost;port=$vPort;dbname=$vData", $vUser, $vPass);
    } catch(PDOException $e){
      echo "Erreur de connection '$e'";
    }

    try{
      $idAnimal = $_POST['idAnimal'];
      //TODO Valider que l'id reçu existe
      if($idAnimal != ""){
          $vSql ="DELETE FROM posologie P  WHERE P.traitement IN (SELECT idTraitement FROM traitement WHERE idAnimal='$idAnimal');";
          $vSt = $vConn->prepare($vSql);
          $vSt->execute();

          $vSql ="DELETE FROM traitement WHERE idAnimal='$idAnimal';";
          $vSt = $vConn->prepare($vSql);
          $vSt->execute();

          $vSql ="DELETE FROM animal WHERE idAnimal='$idAnimal';";
          $vSt = $vConn->prepare($vSql);
          $vSt->execute();

          header('Location: http://tuxa.sme.utc/~bdd0p042/application/animal.php');
      }else
        echo "Il manque une donnée";
    } catch(PDOException $e){
      echo "Erreur de suppresion '$e'";
    }


  ?>
</body>
</html>

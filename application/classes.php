<html>
<head>
  <title>Clinique Vétérinaire</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="stylesheet" type="text/css" href="style.css">
  </head>
<body>
  <h1>Information sur les classes</h1>
  <table border="1">
    <tr>
      <th>Nom Classe</th>
    </tr>
    <?php
    // Connexion à la base de données
    include("connexion.php");

    try{
      $vConn = new PDO("pgsql:host=$vHost;port=$vPort;dbname=$vData", $vUser, $vPass);
    } catch(PDOException $e){
      echo "Erreur de connexion '$e'";
    }

    // Affichage du tableau de données
    $vSql = "select * from ClasseEspece";
    try{
      $vSt = $vConn->prepare($vSql);
      $vSt->execute();
      
      while ($vResult = $vSt->fetch(PDO::FETCH_BOTH)) {
        echo '<tr>';
        echo "<td>$vResult[0]</td>";
        echo '</tr>';
      }

    } catch(PDOException $e){
      echo "Erreur de requete '$e'";
    }
    ?>

  </table>
</body>
</html>

<html>
<head>
  <title>Clinique Vétérinaire</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="stylesheet" type="text/css" href="style.css">
  </head>
<body>
  <?php
    include("connexion.php");
    try{
      $vConn = new PDO("pgsql:host=$vHost;port=$vPort;dbname=$vData", $vUser, $vPass);
    } catch(PDOException $e){
      echo "Erreur de connexion '$e'";
    }
    $nomMolec = $_POST['nomMolec'];

    if($nomMolec == ""){
        echo "Il faut sélectionner un medicament";
        header('Location: http://tuxa.sme.utc/~bdd0p042/application/medicament.php');
      }
  ?>

    <h1>Quantité de <?php echo $nomMolec ?> prescrit</h1>
  <table border="1">
    <tr>
      <th>Medicament</th>
      <th>Quantité</th>
    </tr>
    <?php

    try{
      $vSql ="SELECT P.nomMolec as medicament, COUNT(*)
        FROM Posologie P
        WHERE P.nomMolec='$nomMolec'
        GROUP BY P.nomMolec;
      ";


      $vSt = $vConn->prepare($vSql);
      $vSt->execute();
      while ($vResult = $vSt->fetch(PDO::FETCH_BOTH)) {
        echo '<tr>';
        echo "<td>$vResult[0]</td>";
        echo "<td>$vResult[1]</td>";
        echo '</tr>';
      }
    } catch(PDOException $e){
      echo "Erreur de requete '$e'";
    }
    ?>
  </table>


  <h1>Nombre de comprimés prescrit pour <?php echo $nomMolec ?></h1>
  <table border="1">
    <tr>
      <th>Medicament</th>
      <th>Nb comprimés</th>
    </tr>
    <?php

    try{
      $vSql ="SELECT P.nomMolec, SUM(P.duree*P.nbJourna)
              FROM Posologie P
              WHERE P.nomMolec='$nomMolec'
              GROUP BY P.nomMolec;
        ";


        $vSt = $vConn->prepare($vSql);
        $vSt->execute();
        while ($vResult = $vSt->fetch(PDO::FETCH_BOTH)) {
          echo '<tr>';
          echo "<td>$vResult[0]</td>";
          echo "<td>$vResult[1]</td>";
          echo '</tr>';
        }
    } catch(PDOException $e){
      echo "Erreur de requete '$e'";
    }

    ?>
  </table>
</body>
</html>

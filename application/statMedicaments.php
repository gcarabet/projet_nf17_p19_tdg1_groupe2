<html>
<head>
  <title>Clinique Vétérinaire</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="stylesheet" type="text/css" href="style.css">
  </head>
<body>
  <?php
    include("connexion.php");
    try{
      $vConn = new PDO("pgsql:host=$vHost;port=$vPort;dbname=$vData", $vUser, $vPass);
    } catch(PDOException $e){
      echo "Erreur de connexion '$e'";
    }
  ?>

  <h1>Nombre de comprimés prescrit dans la clinique</h1>
  <table border="1">
    <tr>
      <th>Medicament</th>
      <th>Nb comprimés</th>
    </tr>
    <?php

    try{
      $vSql ="SELECT P.nomMolec, SUM(P.duree*P.nbJourna)
              FROM Posologie P
              GROUP BY P.nomMolec;
        ";


        $vSt = $vConn->prepare($vSql);
        $vSt->execute();
        while ($vResult = $vSt->fetch(PDO::FETCH_BOTH)) {
          echo '<tr>';
          echo "<td>$vResult[0]</td>";
          echo "<td>$vResult[1]</td>";
          echo '</tr>';
        }
    } catch(PDOException $e){
      echo "Erreur de requete '$e'";
    }

    ?>
  </table>
</body>
</html>

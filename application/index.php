<html>
<head>
  <title>Clinique Vétérinaire</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  </head>
<body>
  <h1>Gestion de la clinique</h1>
  <!-- ComboBox Affichage -->
  <div>
    <form method="post" action="affichage.php">
      <p>
        <label> Affiche la/les table(s) </label><br />
        <select name="affiche">
          <option value="all" selected>All</option>
          <option value="clients">Clients</option>
          <option value="veterinaires">Veterinaires</option>
          <option value="assistants">Assistants</option>
          <option value="classes">Classes</option>
          <option value="especes">Especes</option>
          <option value="animaux">Animaux</option>
          <option value="medicaments">Medicaments</option>
          <option value="traitements">Traitements</option>
          <option value="posologie">Posologies</option>
        </select>
        <input type="submit" />
      </p>
    </form>
  </div>
  <!-- ComboBox Ajout -->
  <div>
    <form method="post" action="ajout.php">
      <p>
        <label> Ajout à la table </label><br />
        <select name="ajoute">
          <option value="clients" selected>Clients</option>
          <option value="animaux">Animaux</option>
          <option value="traitements">Traitements</option>
        </select>
        <input type="submit" />
      </p>
    </form>
  </div>
</body>
</html>

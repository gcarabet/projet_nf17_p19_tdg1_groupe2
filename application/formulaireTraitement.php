<html>
<head>
  <title>Clinique Vétérinaire</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="stylesheet" type="text/css" href="style.css">
  </head>
<body>
  <h1>Ajout d'un Traitement</h1>
  <?php
  // Connexion à la base de données
    include("connexion.php");
    $vConn = new PDO("pgsql:host=$vHost;port=$vPort;dbname=$vData", $vUser, $vPass);
  ?>
  <div>
    <form  action="addTraitement.php" method="post">
      <p>Animal:
        <select name="animal" required/>
        <?php
            $vSql ='select idAnimal from Animal;';
            $vSt = $vConn->prepare($vSql);
            $vSt->execute();
            while ($vResult = $vSt->fetch(PDO::FETCH_BOTH)) {
              echo "<option>$vResult[0]</option>";
            }
        ?>
       </select>
      </p>
       <p>ID Vétérinaire:
         <select name="veterinaire" required/>
         <?php
             $vSql ='select idVeterinaire from Veterinaire;';
             $vSt = $vConn->prepare($vSql);
             $vSt->execute();
             while ($vResult = $vSt->fetch(PDO::FETCH_BOTH)) {
               echo "<option>$vResult[0]</option>";
             }
         ?>
        </select>
       </p>
       <p>Médicament:
         <select name="medicament" required/>
         <?php
             $vSql ='select nomMolec from Medicament;';
             $vSt = $vConn->prepare($vSql);
             $vSt->execute();
             while ($vResult = $vSt->fetch(PDO::FETCH_BOTH)) {
               echo "<option>$vResult[0]</option>";
             }
         ?>
        </select>
       </p>
       <p>Date de debut: <input type="date" name="ddebut" required/></p>
       <p>Durée: <input type="number" name="duree" required/></p>
       <p>Quantité par jour: <input type="number" name="qteJour" required/></p>
       <p><input type="submit" value="OK"></p>
    </form>
  </div>
</body>
</html>

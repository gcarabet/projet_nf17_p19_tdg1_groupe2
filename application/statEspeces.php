<html>
<head>
  <title>Clinique Vétérinaire</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="stylesheet" type="text/css" href="style.css">
  </head>
<body>
  <?php
    include("connexion.php");
    try{
      $vConn = new PDO("pgsql:host=$vHost;port=$vPort;dbname=$vData", $vUser, $vPass);
    } catch(PDOException $e){
      echo "Erreur de connexion '$e'";
    }
  ?>

  <h1>Information sur les espèces</h1>
  <table border="1">
    <tr>
      <th>Espèce</th>
      <th>Poids moyen</th>
      <th>Taille moyenne</th>
    </tr>
    <?php

    try{
      $vSql ="SELECT A.especenom, AVG(A.poids), AVG(A.taille)
        FROM Animal A
        GROUP BY A.especenom;
      ";


      $vSt = $vConn->prepare($vSql);
      $vSt->execute();
      while ($vResult = $vSt->fetch(PDO::FETCH_BOTH)) {
        echo '<tr>';
        echo "<td>$vResult[0]</td>";
        echo "<td>$vResult[1]</td>";
        echo "<td>$vResult[2]</td>";
        echo '</tr>';
      }
    } catch(PDOException $e){
      echo "Erreur de requete '$e'";
    }
    ?>
  </table>
</body>
</html>

<?php
			include("connexion.php");
?>

<html>
  <head>
    <title> Remove Client </title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  </head>

  <body>

  <form method = "post" action = "rmClient.php">
       Id Client : 

	<select name = "idClient">
		<?php

			$vSql ='SELECT idClient FROM client;';

			try
			{

		   		$vConn = new PDO("pgsql:host=$vHost;port=$vPort;dbname=$vData", $vUser, $vPass);
				try
				{
			        	$vSt = $vConn->prepare($vSql);
				        $vSt->execute();
				        while ($vResult = $vSt->fetch(PDO::FETCH_ASSOC))
					{
				          echo "<option>$vResult[0]</option>";
				        }
				} 
				catch(PDOException $e)
				{
					echo "Erreur de requete '$e'";
				}
			} 
			catch(PDOException $e)
			{
				echo "Erreur de la connexion '$e'";
			}
			
		?>
	</select>
	<BR/>
	
	Id Animal : 

	<select name = "idCAnimal">
		<?php

			$vSql ='SELECT idAnimal FROM Animal;';

			try
			{

		   		$vConn = new PDO("pgsql:host=$vHost;port=$vPort;dbname=$vData", $vUser, $vPass);
				try
				{
			        	$vSt = $vConn->prepare($vSql);
				        $vSt->execute();
				        while ($vResult = $vSt->fetch(PDO::FETCH_ASSOC))
					{
				          echo "<option>$vResult[0]</option>";
				        }
				} 
				catch(PDOException $e)
				{
					echo "Erreur de requete '$e'";
				}
			} 
			catch(PDOException $e)
			{
				echo "Erreur de la connexion '$e'";
			}
			
		?>
	</select>

	<br/>
	
	Id Traitement : 

	<select name = "idCTraitement">
		<?php

			$vSql ='SELECT idTraitement FROM Traitement;';

			try
			{

		   		$vConn = new PDO("pgsql:host=$vHost;port=$vPort;dbname=$vData", $vUser, $vPass);
				try
				{
			        	$vSt = $vConn->prepare($vSql);
				        $vSt->execute();
				        while ($vResult = $vSt->fetch(PDO::FETCH_ASSOC))
					{
				          echo "<option>$vResult[0]</option>";
				        }
				} 
				catch(PDOException $e)
				{
					echo "Erreur de requete '$e'";
				}
			} 
			catch(PDOException $e)
			{
				echo "Erreur de la connexion '$e'";
			}
			
		?>
	</select>

	<br/>
	<button name = "remove" value = "add" type = "submit"> Remove </button>
  </form>
  </body>

</html>

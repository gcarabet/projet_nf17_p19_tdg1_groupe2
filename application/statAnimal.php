<html>
<head>
  <title>Clinique Vétérinaire</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="stylesheet" type="text/css" href="style.css">
  </head>
<body>
  <?php
    include("connexion.php");
    try{
      $vConn = new PDO("pgsql:host=$vHost;port=$vPort;dbname=$vData", $vUser, $vPass);
    } catch(PDOException $e){
      echo "Erreur de connexion '$e'";
    }
    $idAnimal = $_POST['idAnimal'];

    if($idAnimal == ""){
        echo "Il faut sélectionner un animal";
        header('Location: http://tuxa.sme.utc/~bdd0p042/application/animaux.php');
      }
  ?>

  <h1>Quantité de médicament prescrite à un animal</h1>
  <table border="1">
    <tr>
      <th>Medicament</th>
      <th>Somme</th>
    </tr>
    <?php

    try{
      $vSql ="SELECT P.nomMolec as medicament, COUNT(*) as quantite_prescrite
        FROM Posologie P JOIN Traitement T on P.traitement=T.idTraitement
        WHERE T.idAnimal=' $idAnimal'
        GROUP BY P.nomMolec;
      ";


      $vSt = $vConn->prepare($vSql);
      $vSt->execute();
      while ($vResult = $vSt->fetch(PDO::FETCH_BOTH)) {
        echo '<tr>';
        echo "<td>$vResult[0]</td>";
        echo "<td>$vResult[1]</td>";
        echo '</tr>';
      }
    } catch(PDOException $e){
      echo "Erreur de requete '$e'";
    }
    ?>
  </table>


  <h1>Nombre de comprimés prescrit à un animal </h1>
  <table border="1">
    <tr>
      <th>Medicament</th>
      <th>Nb comprimés</th>
    </tr>
    <?php

    try{
        $vSql ="SELECT P.nomMolec, SUM(P.duree*P.nbJourna) FROM Posologie P
          JOIN Traitement T ON P.traitement=T.idTraitement WHERE
          T.idAnimal='$idAnimal'  group by P.nomMolec;
        ";


        $vSt = $vConn->prepare($vSql);
        $vSt->execute();
        while ($vResult = $vSt->fetch(PDO::FETCH_BOTH)) {
          echo '<tr>';
          echo "<td>$vResult[0]</td>";
          echo "<td>$vResult[1]</td>";
          echo '</tr>';
        }
    } catch(PDOException $e){
      echo "Erreur de requete '$e'";
    }

    ?>
  </table>
</body>
</html>

CREATE TABLE ClasseEspece(
	nomClasse VARCHAR(30) NOT NULL,
	PRIMARY KEY(nomClasse)
);

CREATE TABLE Client(
	idClient SERIAL NOT NULL,
	nom VARCHAR(30) NOT NULL,
	prenom VARCHAR(30) NOT NULL,
	ddn DATE NOT NULL,
	adresse VARCHAR(100) NOT NULL,
	numero INT NOT NULL,
	PRIMARY KEY(idClient),
  CHECK (numero >= 0100000000 AND numero <= 0799999999)
);

CREATE TABLE Assistant(
	idAssistant INT NOT NULL,
	nom VARCHAR(30) NOT NULL,
	prenom VARCHAR(30) NOT NULL,
	ddn DATE NOT NULL,
	adresse VARCHAR(100) NOT NULL,
	numero INT NOT NULL,
	specialite VARCHAR(30),
	PRIMARY KEY(idAssistant),
  FOREIGN KEY(specialite) REFERENCES ClasseEspece(nomClasse),
  CHECK (numero >= 0100000000 AND numero <= 0799999999)
);

CREATE TABLE Veterinaire(
	idVeterinaire INT NOT NULL,
	nom VARCHAR(30) NOT NULL,
	prenom VARCHAR(30) NOT NULL,
	ddn DATE NOT NULL,
	adresse VARCHAR(100) NOT NULL,
	numero INT NOT NULL,
	specialite VARCHAR(30),
	PRIMARY KEY(idVeterinaire),
  FOREIGN KEY(specialite) REFERENCES ClasseEspece(nomClasse),
  CHECK (numero >= 0100000000 AND numero <= 0799999999)
);

CREATE TABLE Espece(
	nomEspece VARCHAR(30) NOT NULL,
	nomClasse VARCHAR(30) NOT NULL,
	PRIMARY KEY(nomEspece),
	FOREIGN KEY(nomClasse) REFERENCES ClasseEspece(nomClasse)
);

CREATE TABLE Animal(
	idAnimal SERIAL NOT NULL,
	nom VARCHAR(30) NOT NULL,
	poids REAL NOT NULL,
	taille REAL NOT NULL,
	naissance DATE,
	idClient INT NOT NULL,
	especeNom VARCHAR(30),
	PRIMARY KEY(idAnimal),
	FOREIGN KEY (idClient) REFERENCES Client(idClient),
	FOREIGN KEY (especeNom) REFERENCES Espece(nomEspece),
	CHECK (POIDS > 0),
  CHECK (TAILLE > 0)
);

CREATE TABLE Medicament(
	nomMolec VARCHAR(30) NOT NULL,
	description VARCHAR(200) NOT NULL,
	PRIMARY KEY(nomMolec)
);

CREATE TABLE Traitement(
	idTraitement SERIAL NOT NULL,
	idAnimal INT NOT NULL,
	idVeterinaire INT NOT NULL,
	PRIMARY KEY(idTraitement),
	FOREIGN KEY(idVeterinaire) REFERENCES Veterinaire(idVeterinaire),
	FOREIGN KEY(idAnimal) REFERENCES Animal(idAnimal)
);

CREATE TABLE Espece_Med(
	nomEspece VARCHAR(30) NOT NULL,
	nomMolec VARCHAR(30) NOT NULL,
  PRIMARY KEY(nomEspece, nomMolec),
	FOREIGN KEY(nomEspece) REFERENCES Espece(nomEspece),
	FOREIGN KEY(nomMolec) REFERENCES Medicament(nomMolec)
);

CREATE TABLE Posologie(
	traitement INT NOT NULL REFERENCES Traitement(idTraitement),
	nomMolec VARCHAR(30) NOT NULL REFERENCES Medicament(nomMolec),
	debut DATE NOT NULL,
	duree INT NOT NULL,
	nbJourna INT NOT NULL,
	PRIMARY KEY(traitement, nomMolec, debut),
  CHECK (duree > 0),
  CHECK (nbJourna > 0)
);

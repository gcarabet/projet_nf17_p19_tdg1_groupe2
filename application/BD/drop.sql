/*  
Les tables doivent être détruites
dans l'ordre inverse de leur création.
*/

DROP TABLE IF EXISTS Posologie;
DROP TABLE IF EXISTS Espece_Med;
DROP TABLE IF EXISTS Traitement;
DROP TABLE IF EXISTS Medicament;
DROP TABLE IF EXISTS Animal;
DROP TABLE IF EXISTS Espece;
DROP TABLE IF EXISTS Veterinaire;
DROP TABLE IF EXISTS Assistant;
DROP TABLE IF EXISTS Client;
DROP TABLE IF EXISTS ClasseEspece;

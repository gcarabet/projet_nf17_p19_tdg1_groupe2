<html>
<head>
  <title>Clinique Vétérinaire</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="stylesheet" type="text/css" href="style.css">
  </head>
<body>
  <h1>Information sur les especes</h1>
  <form action='statEspeces.php' method='post'><input type='submit' value='Statistiques générales'> </form>
  <table border="1">
    <tr>
      <th>Espece</th>
      <th>Classe</th>
      <th>Statistique</th>
    </tr>
    <?php
    // Connexion à la base de données
    include("connexion.php");

    try{
      $vConn = new PDO("pgsql:host=$vHost;port=$vPort;dbname=$vData", $vUser, $vPass);
    } catch(PDOException $e){
      echo "Erreur de connexion '$e'";
    }

    // Affichage du tableau de données
    $vSql = "select * from espece";
    try{
      $vSt = $vConn->prepare($vSql);
      $vSt->execute();

      while ($vResult = $vSt->fetch(PDO::FETCH_BOTH)) {
        echo '<tr>';
        echo "<td>$vResult[0]</td>";
        echo "<td>$vResult[1]</td>";
        echo "<td><form action='statEspece.php' method='post'> <input type='text' name='espece' value=$vResult[0] hidden/> <input type='submit' value='Statistique'></form></td>";
        echo '</tr>';
      }

    } catch(PDOException $e){
      echo "Erreur de requete '$e'";
    }
    ?>

  </table>
</body>
</html>

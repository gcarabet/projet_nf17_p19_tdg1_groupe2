<html>
<head>
  <title>Clinique Vétérinaire</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <?php
    include("connexion.php");
    try{
      $vConn = new PDO("pgsql:host=$vHost;port=$vPort;dbname=$vData", $vUser, $vPass);
    } catch(PDOException $e){
      echo "Erreur de connexion '$e'";
    }
    $espece = $_POST['espece'];

    if($espece == ""){
        echo "Il faut sélectionner une espece";
        header('Location: http://tuxa.sme.utc/~bdd0p042/application/especes.php');
      }
  ?>

    <h1>Poids moyen des <?php echo $espece ?></h1>
  <table border="1">
    <tr>
      <th>Espèce</th>
      <th>Poids moyen</th>
    </tr>
    <?php

    try{
      $vSql ="SELECT A.especenom, AVG(A.poids) FROM Animal A WHERE A.especenom='$espece' GROUP BY A.especenom";


      $vSt = $vConn->prepare($vSql);
      $vSt->execute();
      while ($vResult = $vSt->fetch(PDO::FETCH_BOTH)) {
        echo '<tr>';
        echo "<td>$vResult[0]</td>";
        echo "<td>$vResult[1]</td>";
        echo '</tr>';
      }
    } catch(PDOException $e){
      echo "Erreur de requete '$e'";
    }
    ?>
  </table>

  <h1>Taille moyenne des <?php echo $espece ?></h1>
  <table border="1">
    <tr>
      <th>Espèce</th>
      <th>Taille moyenne</th>
    </tr>
    <?php

    try{
      $vSql ="SELECT A.especenom, AVG(A.taille) FROM Animal A WHERE A.especenom='$espece' GROUP BY A.especenom";


      $vSt = $vConn->prepare($vSql);
      $vSt->execute();
      while ($vResult = $vSt->fetch(PDO::FETCH_BOTH)) {
        echo '<tr>';
        echo "<td>$vResult[0]</td>";
        echo "<td>$vResult[1]</td>";
        echo '</tr>';
      }
    } catch(PDOException $e){
      echo "Erreur de requete '$e'";
    }
    ?>
  </table>
</body>
</html>

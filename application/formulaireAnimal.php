<html>
<head>
  <title>Clinique Vétérinaire</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="stylesheet" type="text/css" href="style.css">
  </head>
<body>
  <h1>Ajout d'un animal</h1>
  <?php
  // Connexion à la base de données
    include("connexion.php");
    $vConn = new PDO("pgsql:host=$vHost;port=$vPort;dbname=$vData", $vUser, $vPass);
  ?>
  <div>
    <form  action="addAnimal.php" method="post">
       <p>Nom de l'animal: <input type="text" name="nom" required/></p>
       <p>Poids: <input type="text" name="poids" required/></p>
       <p>Taille: <input type="text" name="taille" required/></p>
       <p>Date de naissance: <input type="date" name="ddn" required/></p>
       <p>Client:
         <select name="client" required/>
         <?php
             $vSql ='select idClient from Client;';
             $vSt = $vConn->prepare($vSql);
             $vSt->execute();
             while ($vResult = $vSt->fetch(PDO::FETCH_BOTH)) {
               echo "<option>$vResult[0]</option>";
             }
         ?>
        </select>
       </p>
       <p>Espece:
           <select name="espece" required/>
           <?php
               $vSql ='select nomEspece from Espece;';
               $vSt = $vConn->prepare($vSql);
               $vSt->execute();
               while ($vResult = $vSt->fetch(PDO::FETCH_BOTH)) {
                 echo "<option>$vResult[0]</option>";
               }
           ?>
          </select>
       </p>
       <p><input type="submit" value="OK"></p>
    </form>
  </div>
</body>
</html>
